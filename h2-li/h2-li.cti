#
# Generated from file h2-li.txt
# by ck2cti on Thu Sep 24 21:18:09 2015
#
# Transport data from file h2-transport.txt.

units(length = "cm", time = "s", quantity = "mol", act_energy = "K")


ideal_gas(name = "h2-li",
      elements = " H  O  N  Ar  He ",
      species = """ H  H2  O  OH  H2O  O2  HO2  H2O2  N2  AR 
                   HE """,
      reactions = "all",
      transport = "Mix",
      initial_state = state(temperature = 300.0,
                        pressure = OneAtm)    )



#-------------------------------------------------------------------------------
#  Species data 
#-------------------------------------------------------------------------------

species(name = "H",
    atoms = " H:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
                2.547163000E+04,  -4.601176000E-01] ),
       NASA( [ 1000.00,  5000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
                2.547163000E+04,  -4.601176000E-01] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 2.05,
                     well_depth = 145)
       )

species(name = "H2",
    atoms = " H:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.298124000E+00,   8.249442000E-04, 
               -8.143015000E-07,  -9.475434000E-11,   4.134872000E-13,
               -1.012521000E+03,  -3.294094000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.991423000E+00,   7.000644000E-04, 
               -5.633829000E-08,  -9.231578000E-12,   1.582752000E-15,
               -8.350340000E+02,  -1.355110000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 2.92,
                     well_depth = 38,
                     polar = 0.79,
                     rot_relax = 280)
       )

species(name = "O",
    atoms = " O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.946429000E+00,  -1.638166000E-03, 
                2.421032000E-06,  -1.602843000E-09,   3.890696000E-13,
                2.914764000E+04,   2.963995000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.542060000E+00,  -2.755062000E-05, 
               -3.102803000E-09,   4.551067000E-12,  -4.368052000E-16,
                2.923080000E+04,   4.920308000E+00] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 2.75,
                     well_depth = 80)
       )

species(name = "OH",
    atoms = " H:1  O:1 ",
    thermo = (
       NASA( [  200.00,  1000.00], [  4.125305610E+00,  -3.225449390E-03, 
                6.527646910E-06,  -5.798536430E-09,   2.062373790E-12,
                3.346309130E+03,  -6.904329600E-01] ),
       NASA( [ 1000.00,  6000.00], [  2.864728860E+00,   1.056504480E-03, 
               -2.590827580E-07,   3.052186740E-11,  -1.331958760E-15,
                3.683628750E+03,   5.701640730E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 2.75,
                     well_depth = 80)
       )

species(name = "H2O",
    atoms = " H:2  O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.386842000E+00,   3.474982000E-03, 
               -6.354696000E-06,   6.968581000E-09,  -2.506588000E-12,
               -3.020811000E+04,   2.590233000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.672146000E+00,   3.056293000E-03, 
               -8.730260000E-07,   1.200996000E-10,  -6.391618000E-15,
               -2.989921000E+04,   6.862817000E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam = 2.605,
                     well_depth = 572.4,
                     dipole = 1.844,
                     rot_relax = 4)
       )

species(name = "O2",
    atoms = " O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.212936000E+00,   1.127486000E-03, 
               -5.756150000E-07,   1.313877000E-09,  -8.768554000E-13,
               -1.005249000E+03,   6.034738000E+00] ),
       NASA( [ 1000.00,  5000.00], [  3.697578000E+00,   6.135197000E-04, 
               -1.258842000E-07,   1.775281000E-11,  -1.136435000E-15,
               -1.233930000E+03,   3.189166000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 3.458,
                     well_depth = 107.4,
                     polar = 1.6,
                     rot_relax = 3.8)
       )

species(name = "HO2",
    atoms = " H:1  O:2 ",
    thermo = (
       NASA( [  200.00,  1000.00], [  4.301798010E+00,  -4.749120510E-03, 
                2.115828910E-05,  -2.427638940E-08,   9.292251240E-12,
                2.948080400E+02,   3.716662450E+00] ),
       NASA( [ 1000.00,  3500.00], [  4.017210900E+00,   2.239820130E-03, 
               -6.336581500E-07,   1.142463700E-10,  -1.079085350E-14,
                1.118567130E+02,   3.785102150E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam = 3.458,
                     well_depth = 107.4,
                     rot_relax = 1)
       )

species(name = "H2O2",
    atoms = " H:2  O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.388754000E+00,   6.569226000E-03, 
               -1.485013000E-07,  -4.625806000E-09,   2.471515000E-12,
               -1.766315000E+04,   6.785363000E+00] ),
       NASA( [ 1000.00,  5000.00], [  4.573167000E+00,   4.336136000E-03, 
               -1.474689000E-06,   2.348904000E-10,  -1.431654000E-14,
               -1.800696000E+04,   5.011370000E-01] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam = 3.458,
                     well_depth = 107.4,
                     rot_relax = 3.8)
       )

species(name = "N2",
    atoms = " N:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.298677000E+00,   1.408240000E-03, 
               -3.963222000E-06,   5.641515000E-09,  -2.444855000E-12,
               -1.020900000E+03,   3.950372000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.926640000E+00,   1.487977000E-03, 
               -5.684761000E-07,   1.009704000E-10,  -6.753351000E-15,
               -9.227977000E+02,   5.980528000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 3.621,
                     well_depth = 97.53,
                     polar = 1.76,
                     rot_relax = 4)
       )

species(name = "AR",
    atoms = " Ar:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
               -7.453750000E+02,   4.366001000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
               -7.453750000E+02,   4.366001000E+00] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 3.33,
                     well_depth = 136.5)
       )

species(name = "HE",
    atoms = " He:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
               -7.453750000E+02,   9.153488000E-01] ),
       NASA( [ 1000.00,  5000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
               -7.453750000E+02,   9.153489000E-01] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 2.576,
                     well_depth = 10.2)
       )



#-------------------------------------------------------------------------------
#  Reaction data 
#-------------------------------------------------------------------------------

#  Reaction 1
#      1 
reaction(  "H + O2 <=> O + OH",  [3.54700E+15, -0.406, 8353.8])

#  Reaction 2
#      2                                        
reaction(  "O + H2 <=> H + OH",  [5.08000E+04, 2.67, 3165.58])

#  Reaction 3
#      3                                        
reaction(  "H2 + OH <=> H2O + H",  [2.16000E+08, 1.51, 1726.22])

#  Reaction 4
#      4                                        
reaction(  "O + H2O <=> 2 OH",  [2.97000E+06, 2.02, 6743.84])

#  Reaction 5
#      5                                        
three_body_reaction( "H2 + M <=> 2 H + M",  [4.57700E+19, -1.4, 52531.5],
         efficiencies = " AR:0  H2:2.5  H2O:12  HE:0 ")

#  Reaction 6
#      6 
reaction(  "H2 + AR <=> 2 H + AR",  [5.84000E+18, -1.1, 52531.5])

#  Reaction 7
#      7                                        
reaction(  "H2 + HE <=> 2 H + HE",  [5.84000E+18, -1.1, 52531.5])

#  Reaction 8
#      8                          
three_body_reaction( "2 O + M <=> O2 + M",  [6.16500E+15, -0.5, 0],
         efficiencies = " AR:0  H2:2.5  H2O:12  HE:0 ")

#  Reaction 9
#      9                   
reaction(  "2 O + AR <=> O2 + AR",  [1.88600E+13, 0, -899.85])

#  Reaction 10
#     10                                       
reaction(  "2 O + HE <=> O2 + HE",  [1.88600E+13, 0, -899.85])

#  Reaction 11
#     11                                       
three_body_reaction( "O + H + M <=> OH + M",  [4.71400E+18, -1, 0],
         efficiencies = " AR:0.75  H2:2.5  H2O:12  HE:0.75 ")

#  Reaction 12
#     12                     
three_body_reaction( "H + OH + M <=> H2O + M",  [3.80000E+22, -2, 0],
         efficiencies = " AR:0.38  H2:2.5  H2O:12  HE:0.38 ")

#  Reaction 13
#     13 
falloff_reaction( "H + O2 (+ M) <=> HO2 (+ M)",
         kf = [1.47500E+12, 0.6, 0],
         kf0   = [9.04200E+19, -1.5, 247.71],
         falloff = Troe(A = 0.5, T3 = 1e-30, T1 = 1e+30),
         efficiencies = " H2:3  H2O:16  HE:1.2  O2:1.1 ")

#  Reaction 14
#     14                        
reaction(  "HO2 + H <=> H2 + O2",  [1.66000E+13, 0, 414.19])

#  Reaction 15
#     15                                         
reaction(  "HO2 + H <=> 2 OH",  [7.07900E+13, 0, 148.47])

#  Reaction 16
#     16                                         
reaction(  "HO2 + O <=> O2 + OH",  [3.25000E+13, 0, 0])

#  Reaction 17
#     17                                         
reaction(  "HO2 + OH <=> H2O + O2",  [2.89000E+13, 0, -250.13])

#  Reaction 18
#     18                                         
reaction(  "2 HO2 <=> H2O2 + O2",  [4.20000E+14, 0, 6030.2],
         options = ["duplicate"])

#  Reaction 19
#     19                                         
reaction(  "2 HO2 <=> H2O2 + O2",  [1.30000E+11, 0, -819.98],
         options = ["duplicate"])

#  Reaction 20
#     20                                         
falloff_reaction( "H2O2 (+ M) <=> 2 OH (+ M)",
         kf = [2.95100E+14, 0, 24373.4],
         kf0   = [1.20200E+17, 0, 22898.8],
         falloff = Troe(A = 0.5, T3 = 1e-30, T1 = 1e+30),
         efficiencies = " AR:0.64  H2:2.5  H2O:12  HE:0.64 ")

#  Reaction 21
#     21                        
reaction(  "H2O2 + H <=> H2O + OH",  [2.41000E+13, 0, 1997.99])

#  Reaction 22
#     22                                         
reaction(  "H2O2 + H <=> HO2 + H2",  [4.82000E+13, 0, 4001.01])

#  Reaction 23
#     23                                         
reaction(  "H2O2 + O <=> OH + HO2",  [9.55000E+06, 2, 1997.99])

#  Reaction 24
#     24                                         
reaction(  "H2O2 + OH <=> HO2 + H2O",  [1.00000E+12, 0, 0],
         options = ["duplicate"])

#  Reaction 25
#     25                                         
reaction(  "H2O2 + OH <=> HO2 + H2O",  [5.80000E+14, 0, 4809.76],
         options = ["duplicate"])
