This repository stores chemical reaction mechanisms for combustion of hydrocarbon fuels.

All mechanisms included in this repository must provide:
- cti file (Cantera)
- yaml file (generated from Cantera)
- bibtex file
- the paper associated with the mechanism (if there is a paper)