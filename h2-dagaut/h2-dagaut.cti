#
# Generated from file h2-dagaut.txt
# by ck2cti on Thu Sep 24 21:29:49 2015
#
# Transport data from file h2-transport.txt.

units(length = "cm", time = "s", quantity = "mol", act_energy = "K")


ideal_gas(name = "h2-dagaut",
      elements = " H  O  N  Ar",
      species = """ H  O  OH  O2  H2  H2O  HO2  H2O2  N2  AR """,
      reactions = "all",
      transport = "Mix",
      initial_state = state(temperature = 300.0,
                        pressure = OneAtm)    )



#-------------------------------------------------------------------------------
#  Species data 
#-------------------------------------------------------------------------------

species(name = "H",
    atoms = " H:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
                2.547160000E+04,  -4.600000000E-01] ),
       NASA( [ 1000.00,  5000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
                2.547160000E+04,  -4.600000000E-01] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 2.05,
                     well_depth = 145)
       )

species(name = "O",
    atoms = " O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.946430000E+00,  -1.638170000E-03, 
                2.421030000E-06,  -1.602840000E-09,   3.890700000E-13,
                2.914760000E+04,   2.964000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.542060000E+00,  -2.755000000E-05, 
               -3.100000000E-09,   4.550000000E-12,  -4.400000000E-16,
                2.923080000E+04,   4.920000000E+00] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 2.75,
                     well_depth = 80)
       )

species(name = "OH",
    atoms = " H:1  O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.637270000E+00,   1.850900000E-04, 
               -1.676160000E-06,   2.387200000E-09,  -8.431400000E-13,
                3.606800000E+03,   1.358860000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.882730000E+00,   1.013970000E-03, 
               -2.276900000E-07,   2.175000000E-11,  -5.100000000E-16,
                3.886900000E+03,   5.595710000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 2.75,
                     well_depth = 80)
       )

species(name = "O2",
    atoms = " O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.212940000E+00,   1.127490000E-03, 
               -5.756200000E-07,   1.313880000E-09,  -8.768600000E-13,
               -1.005200000E+03,   6.035000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  3.697580000E+00,   6.135200000E-04, 
               -1.258800000E-07,   1.775000000E-11,  -1.140000000E-15,
               -1.233900000E+03,   3.189000000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 3.458,
                     well_depth = 107.4,
                     polar = 1.6,
                     rot_relax = 3.8)
       )

species(name = "H2",
    atoms = " H:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.298120000E+00,   8.249400000E-04, 
               -8.143000000E-07,  -9.475000000E-11,   4.134900000E-13,
               -1.012500000E+03,  -3.294000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.991420000E+00,   7.000600000E-04, 
               -5.634000000E-08,  -9.230000000E-12,   1.580000000E-15,
               -8.350000000E+02,  -1.355000000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 2.92,
                     well_depth = 38,
                     polar = 0.79,
                     rot_relax = 280)
       )

species(name = "H2O",
    atoms = " H:2  O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.386840000E+00,   3.474980000E-03, 
               -6.354700000E-06,   6.968580000E-09,  -2.506590000E-12,
               -3.020810000E+04,   2.590000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.672150000E+00,   3.056290000E-03, 
               -8.730300000E-07,   1.201000000E-10,  -6.390000000E-15,
               -2.989920000E+04,   6.863000000E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam = 2.605,
                     well_depth = 572.4,
                     dipole = 1.844,
                     rot_relax = 4)
       )

species(name = "HO2",
    atoms = " H:1  O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.979960000E+00,   4.996700000E-03, 
               -3.791000000E-06,   2.354190000E-09,  -8.089000000E-13,
                1.762000000E+02,   9.223000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  4.072190000E+00,   2.131300000E-03, 
               -5.308100000E-07,   6.112000000E-11,  -2.840000000E-15,
               -1.580000000E+02,   3.476000000E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam = 3.458,
                     well_depth = 107.4,
                     rot_relax = 1)
       )

species(name = "H2O2",
    atoms = " H:2  O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.388750000E+00,   6.569230000E-03, 
               -1.485000000E-07,  -4.625810000E-09,   2.471510000E-12,
               -1.766310000E+04,   6.785000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  4.573170000E+00,   4.336140000E-03, 
               -1.474690000E-06,   2.348900000E-10,  -1.432000000E-14,
               -1.800700000E+04,   5.010000000E-01] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam = 3.458,
                     well_depth = 107.4,
                     rot_relax = 3.8)
       )

species(name = "N2",
    atoms = " N:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.298680000E+00,   1.408240000E-03, 
               -3.963220000E-06,   5.641510000E-09,  -2.444850000E-12,
               -1.020900000E+03,   3.950000000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.926640000E+00,   1.487980000E-03, 
               -5.684800000E-07,   1.009700000E-10,  -6.750000000E-15,
               -9.228000000E+02,   5.981000000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam = 3.621,
                     well_depth = 97.53,
                     polar = 1.76,
                     rot_relax = 4)
       )
       
species(name = "AR",
    atoms = " Ar:1 ",
    thermo = (
       NASA( [  200.00,  1000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
               -7.453750000E+02,   4.379674900E+00] ),
       NASA( [ 1000.00,  6000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
               -7.453750000E+02,   4.379674900E+00] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam = 3.33,
                     well_depth = 136.5)
       )


#-------------------------------------------------------------------------------
#  Reaction data 
#-------------------------------------------------------------------------------

#  Reaction 1
three_body_reaction( "2 H + M <=> H2 + M",  [7.31000E+17, -1, 0],
         efficiencies = " H2O:16.25 ")

#  Reaction 2
three_body_reaction( "2 O + M <=> O2 + M",  [1.14000E+17, -1, 0],
         efficiencies = " H2O:16.25 ")

#  Reaction 3
three_body_reaction( "O + H + M <=> OH + M",  [6.20000E+16, -0.6, 0],
         efficiencies = " H2O:16.25 ")

#  Reaction 4
reaction(  "H2 + O2 <=> 2 OH",  [1.70000E+13, 0, 24046.3])

#  Reaction 5
reaction(  "O + H2 <=> OH + H",  [1.10000E+04, 2.8, 2980.37])

#  Reaction 6
reaction(  "H + O2 <=> OH + O",  [1.90000E+14, 0, 8461])

#  Reaction 7
three_body_reaction( "H + O2 + M <=> HO2 + M",  [8.00000E+17, -0.8, 0],
         efficiencies = " H2O:16.25 ")

#  Reaction 8
three_body_reaction( "H + OH + M <=> H2O + M",  [8.61500E+21, -2, 0],
         efficiencies = " H2O:16.25 ")

#  Reaction 9
reaction(  "H2 + OH <=> H2O + H",  [2.16100E+08, 1.51, 1726.22])

#  Reaction 10
reaction(  "H2O + O <=> 2 OH",  [1.50000E+10, 1.14, 8686.46])

#  Reaction 11
reaction(  "HO2 + OH <=> H2O + O2",  [2.89000E+13, 0, -250.13])

#  Reaction 12
reaction(  "HO2 + O <=> OH + O2",  [1.81000E+13, 0, -201.31])

#  Reaction 13
reaction(  "H + HO2 <=> H2 + O2",  [4.28000E+13, 0, 710.12])

#  Reaction 14
reaction(  "H + HO2 <=> 2 OH",  [1.69000E+14, 0, 439.86])

#  Reaction 15
reaction(  "H + HO2 <=> H2O + O",  [3.01000E+13, 0, 866.13])

#  Reaction 16
reaction(  "2 HO2 <=> H2O2 + O2",  [4.07500E+02, 3.321, 995.97])

#  Reaction 17
reaction(  "2 OH <=> H2O2",  [1.32900E+16, -1.462, 30.7])

#  Reaction 18
reaction(  "H2O2 + OH <=> HO2 + H2O",  [5.80000E+14, 0, 4809.76])

#  Reaction 19
reaction(  "H2O2 + H <=> HO2 + H2",  [1.70000E+12, 0, 1887.27])

#  Reaction 20
reaction(  "H2O2 + H <=> H2O + OH",  [1.00000E+13, 0, 1806.74])

#  Reaction 21
reaction(  "H2O2 + O <=> HO2 + OH",  [2.80000E+13, 0, 3220.94])
