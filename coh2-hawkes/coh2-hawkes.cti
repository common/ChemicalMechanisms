#
# Generated from file chem.inp
# by ck2cti on Tue Mar 24 08:27:35 2009
#
# Transport data from file tran.dat.

units(length = "cm", time = "s", quantity = "mol", act_energy = "cal/mol")


ideal_gas(name = "gas",
      elements = " C  H  O  N ",
      species = """ H2  O2  O  OH  H2O  H  HO2  CO  CO2  HCO 
                   N2 """,
      reactions = "all",
      transport = "Mix",
      initial_state = state(temperature = 300.0,
                        pressure = OneAtm)    )



#-------------------------------------------------------------------------------
#  Species data 
#-------------------------------------------------------------------------------

species(name = "H2",
    atoms = " H:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.298124000E+00,   8.249442000E-04, 
               -8.143015000E-07,  -9.475434000E-11,   4.134872000E-13,
               -1.012521000E+03,  -3.294094000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.991423000E+00,   7.000644000E-04, 
               -5.633829000E-08,  -9.231578000E-12,   1.582752000E-15,
               -8.350340000E+02,  -1.355110000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam =     2.92,
                     well_depth =    38.00,
                     polar =     0.79,
                     rot_relax =   280.00),
    note = "121286"
       )

species(name = "O2",
    atoms = " O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.212936000E+00,   1.127486000E-03, 
               -5.756150000E-07,   1.313877000E-09,  -8.768554000E-13,
               -1.005249000E+03,   6.034738000E+00] ),
       NASA( [ 1000.00,  5000.00], [  3.697578000E+00,   6.135197000E-04, 
               -1.258842000E-07,   1.775281000E-11,  -1.136435000E-15,
               -1.233930000E+03,   3.189166000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam =     3.46,
                     well_depth =   107.40,
                     polar =     1.60,
                     rot_relax =     3.80),
    note = "121386"
       )

species(name = "O",
    atoms = " O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.946429000E+00,  -1.638166000E-03, 
                2.421032000E-06,  -1.602843000E-09,   3.890696000E-13,
                2.914764000E+04,   2.963995000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.542060000E+00,  -2.755062000E-05, 
               -3.102803000E-09,   4.551067000E-12,  -4.368052000E-16,
                2.923080000E+04,   4.920308000E+00] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam =     2.75,
                     well_depth =    80.00),
    note = "120186"
       )

species(name = "OH",
    atoms = " O:1  H:1 ",
    thermo = (
       NASA( [  200.00,  1000.00], [  4.125305610E+00,  -3.225449390E-03, 
                6.527646910E-06,  -5.798536430E-09,   2.062373790E-12,
                3.346309130E+03,  -6.904329600E-01] ),
       NASA( [ 1000.00,  6000.00], [  2.864728860E+00,   1.056504480E-03, 
               -2.590827580E-07,   3.052186740E-11,  -1.331958760E-15,
                3.683628750E+03,   5.701640730E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam =     2.75,
                     well_depth =    80.00),
    note = "S 9/01"
       )

species(name = "H2O",
    atoms = " H:2  O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.386842000E+00,   3.474982000E-03, 
               -6.354696000E-06,   6.968581000E-09,  -2.506588000E-12,
               -3.020811000E+04,   2.590233000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.672146000E+00,   3.056293000E-03, 
               -8.730260000E-07,   1.200996000E-10,  -6.391618000E-15,
               -2.989921000E+04,   6.862817000E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam =     2.60,
                     well_depth =   572.40,
                     dipole =     1.84,
                     rot_relax =     4.00),
    note = "20387"
       )

species(name = "H",
    atoms = " H:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
                2.547163000E+04,  -4.601176000E-01] ),
       NASA( [ 1000.00,  5000.00], [  2.500000000E+00,   0.000000000E+00, 
                0.000000000E+00,   0.000000000E+00,   0.000000000E+00,
                2.547163000E+04,  -4.601176000E-01] )
             ),
    transport = gas_transport(
                     geom = "atom",
                     diam =     2.05,
                     well_depth =   145.00),
    note = "120186"
       )

species(name = "HO2",
    atoms = " H:1  O:2 ",
    thermo = (
       NASA( [  200.00,  1000.00], [  4.301798010E+00,  -4.749120510E-03, 
                2.115828910E-05,  -2.427638940E-08,   9.292251240E-12,
                2.948080400E+02,   3.716662450E+00] ),
       NASA( [ 1000.00,  3500.00], [  4.017210900E+00,   2.239820130E-03, 
               -6.336581500E-07,   1.142463700E-10,  -1.079085350E-14,
                1.118567130E+02,   3.785102150E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam =     3.46,
                     well_depth =   107.40,
                     rot_relax =     1.00),
    note = "L 5/89"
       )

species(name = "CO",
    atoms = " C:1  O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.262452000E+00,   1.511941000E-03, 
               -3.881755000E-06,   5.581944000E-09,  -2.474951000E-12,
               -1.431054000E+04,   4.848897000E+00] ),
       NASA( [ 1000.00,  5000.00], [  3.025078000E+00,   1.442689000E-03, 
               -5.630828000E-07,   1.018581000E-10,  -6.910952000E-15,
               -1.426835000E+04,   6.108218000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam =     3.65,
                     well_depth =    98.10,
                     polar =     1.95,
                     rot_relax =     1.80),
    note = "121286"
       )

species(name = "CO2",
    atoms = " C:1  O:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.275725000E+00,   9.922072000E-03, 
               -1.040911000E-05,   6.866687000E-09,  -2.117280000E-12,
               -4.837314000E+04,   1.018849000E+01] ),
       NASA( [ 1000.00,  5000.00], [  4.453623000E+00,   3.140169000E-03, 
               -1.278411000E-06,   2.393997000E-10,  -1.669033000E-14,
               -4.896696000E+04,  -9.553959000E-01] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam =     3.76,
                     well_depth =   244.00,
                     polar =     2.65,
                     rot_relax =     2.10),
    note = "121286"
       )

species(name = "HCO",
    atoms = " H:1  C:1  O:1 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  2.898330000E+00,   6.199147000E-03, 
               -9.623084000E-06,   1.089825000E-08,  -4.574885000E-12,
                4.159922000E+03,   8.983614000E+00] ),
       NASA( [ 1000.00,  5000.00], [  3.557271000E+00,   3.345573000E-03, 
               -1.335006000E-06,   2.470573000E-10,  -1.713851000E-14,
                3.916324000E+03,   5.552299000E+00] )
             ),
    transport = gas_transport(
                     geom = "nonlinear",
                     diam =     3.59,
                     well_depth =   498.00),
    note = "121286"
       )

species(name = "N2",
    atoms = " N:2 ",
    thermo = (
       NASA( [  300.00,  1000.00], [  3.298677000E+00,   1.408240000E-03, 
               -3.963222000E-06,   5.641515000E-09,  -2.444855000E-12,
               -1.020900000E+03,   3.950372000E+00] ),
       NASA( [ 1000.00,  5000.00], [  2.926640000E+00,   1.487977000E-03, 
               -5.684761000E-07,   1.009704000E-10,  -6.753351000E-15,
               -9.227977000E+02,   5.980528000E+00] )
             ),
    transport = gas_transport(
                     geom = "linear",
                     diam =     3.62,
                     well_depth =    97.53,
                     polar =     1.76,
                     rot_relax =     4.00),
    note = "121286"
       )



#-------------------------------------------------------------------------------
#  Reaction data 
#-------------------------------------------------------------------------------

#  Reaction 1
#  ************ H2-O2 Chain Reactions ********************** 
#  Hessler, J. Phys. Chem. A, 102:4517 (1998) 
reaction(  "H + O2 <=> O + OH",  [3.54700E+15, -0.406, 16599])

#  Reaction 2
#  Sutherland et al., 21st Symposium, p. 929 (1986) 
reaction(  "O + H2 <=> H + OH",  [5.08000E+04, 2.67, 6290])

#  Reaction 3
#  Michael and Sutherland, J. Phys. Chem. 92:3853 (1988) 
reaction(  "H2 + OH <=> H2O + H",  [2.16000E+08, 1.51, 3430])

#  Reaction 4
#  Sutherland et al., 23rd Symposium, p. 51 (1990) 
reaction(  "O + H2O <=> OH + OH",  [2.97000E+06, 2.02, 13400])

#  Reaction 5
#  *************** H2-O2 Dissociation Reactions ****************** 
#  Tsang and Hampson, J. Phys. Chem. Ref. Data, 15:1087 (1986) 
three_body_reaction( "H2 + M <=> H + H + M",  [4.57700E+19, -1.4, 104380],
         efficiencies = " CO:1.9  CO2:3.8  H2:2.5  H2O:12 ")

#  Reaction 6
#  Tsang and Hampson, J. Phys. Chem. Ref. Data, 15:1087 (1986) 
three_body_reaction( "O + O + M <=> O2 + M",  [6.16500E+15, -0.5, 0],
         efficiencies = " CO:1.9  CO2:3.8  H2:2.5  H2O:12 ")

#  Reaction 7
#  Tsang and Hampson, J. Phys. Chem. Ref. Data, 15:1087 (1986) 
three_body_reaction( "O + H + M <=> OH + M",  [4.71400E+18, -1, 0],
         efficiencies = " CO:1.9  CO2:3.8  H2:2.5  H2O:12 ")

#  Reaction 8
#  Tsang and Hampson, J. Phys. Chem. Ref. Data, 15:1087 (1986) 
# H+OH+M=H2O+M              2.212E+22 -2.00  0.000E+00 
three_body_reaction( "H + OH + M <=> H2O + M",  [3.80000E+22, -2, 0],
         efficiencies = " CO:1.9  CO2:3.8  H2:2.5  H2O:12 ")

#  Reaction 9
# ************** Formation and Consumption of HO2****************** 
#  Cobos et al., J. Phys. Chem. 89:342 (1985) for kinf 
#  Michael, et al., J. Phys. Chem. A, 106:5297 (2002) for k0 
# ****************************************************************************** 
#  MAIN BATH GAS IS N2 (comment this reaction otherwise) 
falloff_reaction( "H + O2 (+ M) <=> HO2 (+ M)",
         kf = [1.47500E+12, 0.6, 0],
         kf0   = [6.36600E+20, -1.72, 524.8],
         falloff = Troe(A = 0.8, T3 = 1e-30, T1 = 1e+30),
         efficiencies = " CO:1.9  CO2:3.8  H2:2  H2O:11  O2:0.78 ")

#  Reaction 10
#  Tsang and Hampson, J. Phys. Chem. Ref. Data, 15:1087 (1986) [modified] 
reaction(  "HO2 + H <=> H2 + O2",  [1.66000E+13, 0, 823])

#  Reaction 11
#  Tsang and Hampson, J. Phys. Chem. Ref. Data, 15:1087 (1986) [modified] 
reaction(  "HO2 + H <=> OH + OH",  [7.07900E+13, 0, 295])

#  Reaction 12
#  Baulch et al., J. Phys. Chem. Ref Data, 21:411 (1992) 
reaction(  "HO2 + O <=> O2 + OH",  [3.25000E+13, 0, 0])

#  Reaction 13
#  Keyser, J. Phys. Chem. 92:1193 (1988) 
reaction(  "HO2 + OH <=> H2O + O2",  [2.89000E+13, 0, -497])

#  Reaction 14
# ************** CO/HCO REACTIONS ***************** 
#  Troe, 15th Symposium 
falloff_reaction( "CO + O (+ M) <=> CO2 (+ M)",
         kf = [1.80000E+10, 0, 2384],
         kf0   = [1.55000E+24, -2.79, 4191],
         efficiencies = " CO:1.9  CO2:3.8  H2:2.5  H2O:12 ")

#  Reaction 15
#  Fit of Westmoreland, AiChe J., 1986, rel. to N2 - Tim adjusted from MTA's 
#  rate constant, which was rel to Ar. 
#  Tsang and Hampson, JPC Ref. Data, 15:1087 (1986) 
reaction(  "CO + O2 <=> CO2 + O",  [2.53000E+12, 0, 47700])

#  Reaction 16
#  This rate constant is modified per an updated value for HO2+HO2=H2O2+OH 
reaction(  "CO + HO2 <=> CO2 + OH",  [3.01000E+13, 0, 23000])

#  Reaction 17
#  This study (2004) by matching literature experiment results 
reaction(  "CO + OH <=> CO2 + H",  [2.22900E+05, 1.89, -1158.7])

#  Reaction 18
#  This study (2004) by matching literature experiment results 
three_body_reaction( "HCO + M <=> H + CO + M",  [4.74850E+11, 0.659, 14874],
         efficiencies = " CO:1.9  CO2:3.8  H2:2.5  H2O:6 ")

#  Reaction 19
#  Timonen et al., JPC, 92:651 (1988) 
reaction(  "HCO + O2 <=> CO + HO2",  [7.58000E+12, 0, 410])

#  Reaction 20
#  Timonen et al., JPC, 91:692 (1987) 
reaction(  "HCO + H <=> CO + H2",  [7.23000E+13, 0, 0])

#  Reaction 21
#  All reactions from Tsang and Hampson, JPC Ref. Data, 15:1087 (1986) 
reaction(  "HCO + O <=> CO2 + H",  [3.00000E+13, 0, 0])
