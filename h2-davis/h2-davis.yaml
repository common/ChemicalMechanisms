generator: cti2yaml
cantera-version: 2.6.0a2
date: Thu, 15 Jul 2021 15:12:56 -0600
input-files: [h2-davis.cti]

units: {length: cm, quantity: mol, activation-energy: cal/mol}

phases:
- name: h2-davis
  thermo: ideal-gas
  elements: [O, H, N, Ar, He]
  species: [H2, H, AR, N2, H2O2, O, OH, HO2, H2O, O2, HE]
  kinetics: gas
  reactions: all
  transport: mixture-averaged
  state:
    T: 300.0
    P: 1.01325e+05

species:
- name: H2
  composition: {H: 2}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [2.34433112, 7.98052075e-03, -1.9478151e-05, 2.01572094e-08, -7.37611761e-12,
      -917.935173, 0.683010238]
    - [3.3372792, -4.94024731e-05, 4.99456778e-07, -1.79566394e-10, 2.00255376e-14,
      -950.158922, -3.20502331]
  transport:
    model: gas
    geometry: linear
    diameter: 2.92
    well-depth: 38
    polarizability: 0.79
    rotational-relaxation: 280
  note: TPIS78
- name: H
  composition: {H: 1}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [2.5, 7.05332819e-13, -1.99591964e-15, 2.30081632e-18, -9.27732332e-22,
      2.54736599e+04, -0.446682853]
    - [2.50000001, -2.30842973e-11, 1.61561948e-14, -4.73515235e-18, 4.98197357e-22,
      2.54736599e+04, -0.446682914]
  transport:
    model: gas
    geometry: atom
    diameter: 2.05
    well-depth: 145
  note: L 7/88
- name: AR
  composition: {Ar: 1}
  thermo:
    model: NASA7
    temperature-ranges: [300.0, 1000.0, 5000.0]
    data:
    - [2.5, 0.0, 0.0, 0.0, 0.0, -745.375, 4.366]
    - [2.5, 0.0, 0.0, 0.0, 0.0, -745.375, 4.366]
  transport:
    model: gas
    geometry: atom
    diameter: 3.33
    well-depth: 136.5
  note: '120186'
- name: N2
  composition: {N: 2}
  thermo:
    model: NASA7
    temperature-ranges: [300.0, 1000.0, 5000.0]
    data:
    - [3.298677, 1.4082404e-03, -3.963222e-06, 5.641515e-09, -2.444854e-12,
      -1020.8999, 3.950372]
    - [2.92664, 1.4879768e-03, -5.68476e-07, 1.0097038e-10, -6.753351e-15,
      -922.7977, 5.980528]
  transport:
    model: gas
    geometry: linear
    diameter: 3.621
    well-depth: 97.53
    polarizability: 1.76
    rotational-relaxation: 4
  note: '121286'
- name: HE
  composition: {He: 1}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 6000.0]
    data:
    - [2.5, 0.0, 0.0, 0.0, 0.0, -745.375, 0.928723974]
    - [2.5, 0.0, 0.0, 0.0, 0.0, -745.375, 0.928723974]
  transport:
    model: gas
    geometry: atom
    diameter: 2.576
    well-depth: 10.2
  note: L10/90
- name: O
  composition: {O: 1}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [3.1682671, -3.27931884e-03, 6.64306396e-06, -6.12806624e-09, 2.11265971e-12,
      2.91222592e+04, 2.05193346]
    - [2.56942078, -8.59741137e-05, 4.19484589e-08, -1.00177799e-11, 1.22833691e-15,
      2.92175791e+04, 4.78433864]
  transport:
    model: gas
    geometry: atom
    diameter: 2.75
    well-depth: 80
  note: L 1/90
- name: OH
  composition: {O: 1, H: 1}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 6000.0]
    data:
    - [4.12530561, -3.22544939e-03, 6.52764691e-06, -5.79853643e-09, 2.06237379e-12,
      3381.53812, -0.69043296]
    - [2.86472886, 1.05650448e-03, -2.59082758e-07, 3.05218674e-11, -1.33195876e-15,
      3718.85774, 5.70164073]
  transport:
    model: gas
    geometry: linear
    diameter: 2.75
    well-depth: 80
  note: S 9/01
- name: HO2
  composition: {H: 1, O: 2}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [4.30179801, -4.74912051e-03, 2.11582891e-05, -2.42763894e-08, 9.29225124e-12,
      294.80804, 3.71666245]
    - [4.0172109, 2.23982013e-03, -6.3365815e-07, 1.1424637e-10, -1.07908535e-14,
      111.856713, 3.78510215]
  transport:
    model: gas
    geometry: nonlinear
    diameter: 3.458
    well-depth: 107.4
    rotational-relaxation: 1
  note: L 5/89
- name: H2O
  composition: {H: 2, O: 1}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [4.19864056, -2.0364341e-03, 6.52040211e-06, -5.48797062e-09, 1.77197817e-12,
      -3.02937267e+04, -0.849032208]
    - [3.03399249, 2.17691804e-03, -1.64072518e-07, -9.7041987e-11, 1.68200992e-14,
      -3.00042971e+04, 4.9667701]
  transport:
    model: gas
    geometry: nonlinear
    diameter: 2.605
    well-depth: 572.4
    dipole: 1.844
    rotational-relaxation: 4
  note: L 8/89
- name: O2
  composition: {O: 2}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [3.78245636, -2.99673416e-03, 9.84730201e-06, -9.68129509e-09, 3.24372837e-12,
      -1063.94356, 3.65767573]
    - [3.28253784, 1.48308754e-03, -7.57966669e-07, 2.09470555e-10, -2.16717794e-14,
      -1088.45772, 5.45323129]
  transport:
    model: gas
    geometry: linear
    diameter: 3.458
    well-depth: 107.4
    polarizability: 1.6
    rotational-relaxation: 3.8
  note: TPIS89
- name: H2O2
  composition: {H: 2, O: 2}
  thermo:
    model: NASA7
    temperature-ranges: [200.0, 1000.0, 3500.0]
    data:
    - [4.27611269, -5.42822417e-04, 1.67335701e-05, -2.15770813e-08, 8.62454363e-12,
      -1.77025821e+04, 3.43505074]
    - [4.16500285, 4.90831694e-03, -1.90139225e-06, 3.71185986e-10, -2.87908305e-14,
      -1.78617877e+04, 2.91615662]
  transport:
    model: gas
    geometry: nonlinear
    diameter: 3.458
    well-depth: 107.4
    rotational-relaxation: 3.8
  note: L 7/88

reactions:
- equation: H + O2 <=> O + OH  # Reaction 1
  rate-constant: {A: 2.644e+16, b: -0.6707, Ea: 17041}
- equation: O + H2 <=> H + OH  # Reaction 2
  rate-constant: {A: 4.589e+04, b: 2.7, Ea: 6260}
- equation: OH + H2 <=> H + H2O  # Reaction 3
  rate-constant: {A: 1.734e+08, b: 1.51, Ea: 3430}
- equation: 2 OH <=> O + H2O  # Reaction 4
  rate-constant: {A: 3.973e+04, b: 2.4, Ea: -2110}
- equation: 2 H + M <=> H2 + M  # Reaction 5
  type: three-body
  rate-constant: {A: 1.78e+18, b: -1, Ea: 0}
  efficiencies: {AR: 0.63, H2: 0, H2O: 0, HE: 0.63}
- equation: 2 H + H2 <=> 2 H2  # Reaction 6
  rate-constant: {A: 9.0e+16, b: -0.6, Ea: 0}
- equation: 2 H + H2O <=> H2 + H2O  # Reaction 7
  rate-constant: {A: 5.624e+19, b: -1.25, Ea: 0}
- equation: H + OH + M <=> H2O + M  # Reaction 8
  type: three-body
  rate-constant: {A: 4.4e+22, b: -2, Ea: 0}
  efficiencies: {AR: 0.38, H2: 2, H2O: 6.3, HE: 0.38}
- equation: O + H + M <=> OH + M  # Reaction 9
  type: three-body
  rate-constant: {A: 9.428e+18, b: -1, Ea: 0}
  efficiencies: {AR: 0.7, H2: 2, H2O: 12, HE: 0.7}
- equation: 2 O + M <=> O2 + M  # Reaction 10
  type: three-body
  rate-constant: {A: 1.2e+17, b: -1, Ea: 0}
  efficiencies: {AR: 0.83, H2: 2.4, H2O: 15.4, HE: 0.83}
- equation: H + O2 (+ M) <=> HO2 (+ M)  # Reaction 11
  type: falloff
  low-P-rate-constant: {A: 6.328e+19, b: -1.4, Ea: 0}
  high-P-rate-constant: {A: 5.116e+12, b: 0.44, Ea: 0}
  Troe: {A: 0.5, T3: 1.0e-30, T1: 1.0e+30}
  efficiencies: {AR: 0.4, H2: 0.75, H2O: 11.89, HE: 0.46, O2: 0.85}
- equation: H2 + O2 <=> HO2 + H  # Reaction 12
  rate-constant: {A: 5.916e+05, b: 2.433, Ea: 53502}
- equation: 2 OH (+ M) <=> H2O2 (+ M)  # Reaction 13
  type: falloff
  low-P-rate-constant: {A: 2.01e+17, b: -0.584, Ea: -2293}
  high-P-rate-constant: {A: 1.11e+14, b: -0.37, Ea: 0}
  Troe: {A: 0.7346, T3: 94, T1: 1756, T2: 5182}
  efficiencies: {AR: 0.7, H2: 2, H2O: 6, HE: 0.7}
- equation: HO2 + H <=> O + H2O  # Reaction 14
  rate-constant: {A: 3.97e+12, b: 0, Ea: 671}
- equation: HO2 + H <=> 2 OH  # Reaction 15
  rate-constant: {A: 7.485e+13, b: 0, Ea: 295}
- equation: HO2 + O <=> OH + O2  # Reaction 16
  rate-constant: {A: 4.0e+13, b: 0, Ea: 0}
- equation: HO2 + OH <=> O2 + H2O  # Reaction 17
  rate-constant: {A: 2.375e+13, b: 0, Ea: -500}
  duplicate: true
- equation: HO2 + OH <=> O2 + H2O  # Reaction 18
  rate-constant: {A: 1.0e+16, b: 0, Ea: 17330}
  duplicate: true
- equation: 2 HO2 <=> O2 + H2O2  # Reaction 19
  rate-constant: {A: 1.3e+11, b: 0, Ea: -1630}
  duplicate: true
- equation: 2 HO2 <=> O2 + H2O2  # Reaction 20
  rate-constant: {A: 3.658e+14, b: 0, Ea: 12000}
  duplicate: true
- equation: H2O2 + H <=> HO2 + H2  # Reaction 21
  rate-constant: {A: 6.05e+06, b: 2, Ea: 5200}
- equation: H2O2 + H <=> OH + H2O  # Reaction 22
  rate-constant: {A: 2.41e+13, b: 0, Ea: 3970}
- equation: H2O2 + O <=> OH + HO2  # Reaction 23
  rate-constant: {A: 9.63e+06, b: 2, Ea: 3970}
- equation: H2O2 + OH <=> HO2 + H2O  # Reaction 24
  rate-constant: {A: 2.0e+12, b: 0, Ea: 427}
  duplicate: true
- equation: H2O2 + OH <=> HO2 + H2O  # Reaction 25
  rate-constant: {A: 2.67e+41, b: -7, Ea: 37600}
  duplicate: true
